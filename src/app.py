#!/usr/bin/python
from flask import Flask, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager 
from flask_migrate import Migrate, MigrateCommand
from flask import render_template
from flask import request

app = Flask(__name__)
app.debug=True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:root@localhost/testdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

class User(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	username = db.Column(db.String(80), unique = True)
	email = db.Column(db.String(120), unique = True)

	def __init__(self, username, email):
		self.username = username
		self.email = email


	def __repr__(self):
		return '<User %r>' % self.username

db.create_all()

@app.route('/')
def index():
	myUser = User.query.all()
	# oneItem = User.query.filter_by(username ="test2").all()
	return render_template('add_user.html', myUser=myUser)

@app.route('/post_user',methods=['POST'])
def post_user():
	user = User(request.form['username'],request.form['email'])
	db.session.add(user) # creating object 
	db.session.commit()  # saving in the db
	return redirect(url_for('index'))

@app.route('/profile/<username>')
def user_profile(username):
	user = User.query.filter_by(username =username).first()
	return render_template('profile.html', user=user)

@app.route('/update/<id>')
def update_user(id):
	user = User.query.filter_by(id =id).first()
	return render_template('update.html', user=user)

@app.route('/update_user',methods=['POST'])
def update():
	user = User.query.filter_by(id = request.form['id']).first()
	user.username = request.form['username']  
	user.email = request.form['email']
	db.session.commit()
	return redirect(url_for('index'))

@app.route('/delete_user/<id>')
def delete_user(id):
	user = User.query.filter_by(id = id).first()
	db.session.delete(user)
	db.session.commit()
	return redirect(url_for('index'))


if __name__=='__main__':
	app.run()
